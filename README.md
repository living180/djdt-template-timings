[ Copyright 2013-2018 Tom Forbes                                                     ]::
[ Copyright 2022 Daniel Harding                                                      ]::
[ Distributed as part of the djdt-template-timings project under the terms of the    ]::
[ MIT license.                                                                       ]::

# DjDT Template Timings

DjDT Template Timings is a panel for the [Django Debug Toolbar][o1] that gives an
in-depth breakdown of the time it takes to render [Django][o2] [templates][o3] when
processing a request (including templates rendered via the [``{% extends %}``][o4] and
[``{% include %}``][o5] template tags).  If the Django Debug Toolbar [SQL panel][o6] is
enabled, DjDT Template Timings will also report the number of SQL queries executed while
rendering templates.


![DjDT Template Timings Screenshot](./screenshot.png)


[o1]: https://github.com/jazzband/django-debug-toolbar

[o2]: https://www.djangoproject.com/

[o3]: https://docs.djangoproject.com/en/stable/topics/templates/

[o4]: https://docs.djangoproject.com/en/stable/ref/templates/builtins/#extends

[o5]: https://docs.djangoproject.com/en/stable/ref/templates/builtins/#includes

[o6]: https://django-debug-toolbar.readthedocs.io/en/stable/panels.html#sql


## Installation

The recommend way to install DjDT Template Timings is via [pip][i1]:

    python -m pip install djdt-template-timings

Next, add `"djdt_template_timings"` to the Django project's [`INSTALLED_APPS`][i2]
setting:

    INSTALLED_APPS = [
        # ...
        "djdt_template_timings",
        # ...
    ]

Finally, add `"djdt_template_timings.panel.TemplateTimingsPanel"` to the Django
project's [`DEBUG_TOOLBAR_PANELS`][i3] settings:

    DEBUG_TOOLBAR_PANELS = [
        # ...
        "djdt_template_timings.panel.TemplateTimingsPanel",
        # ...
    ]


[i1]: https://pip.pypa.io/

[i2]: https://docs.djangoproject.com/en/stable/ref/settings/#installed-apps

[i3]: https://django-debug-toolbar.readthedocs.io/en/stable/configuration.html#debug-toolbar-panels


## Configuration

DjDT Template Timings is set up to work out of the box with default settings. However,
it can be customized using a configuration mechanism similar to the Django Debug
Toolbar's [`DEBUG_TOOLBAR_CONFIG`][c1]. To modify the default configuration, add a
``DJDT_TEMPLATE_TIMINGS_CONFIG`` setting to the Django project's settings module. This
setting must be a dictionary, which currently supports a single option:

* ``IGNORED_TEMPLATES``

  Default: ``["debug_toolbar/*"]``

  A list of glob-style patterns of template names to ignore when recording timing
  metrics.


Example configuration:

    DJDT_TEMPLATE_TIMINGS_CONFIG = {
        "IGNORED_TEMPLATES": [
            "debug_toolbar/*",
        ],
    }


[c1]: https://django-debug-toolbar.readthedocs.io/en/stable/configuration.html#debug-toolbar-config


## Licensing

DjDT Template Timings is distributed under the terms of the [MIT license][l1].  A copy
of the license text is available in the [LICENSE][l2] file.


[l1]: https://opensource.org/licenses/MIT

[l2]: ./LICENSE


## Acknowledgements

DjDT Template Timings started out as a fork of Tom Forbes'
[django-debug-toolbar-template-timings][a1] project.  It has now been almost completely
rewritten, but this project would have never come to fruition without that base upon
which to build.


[a1]: https://github.com/orf/django-debug-toolbar-template-timings
