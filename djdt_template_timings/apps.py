# Copyright 2022 Daniel Harding
# Distributed as part of the djdt-template-timings project under the terms of the MIT
# license.

from django.apps import AppConfig


class TemplateTimingsAppConfig(AppConfig):
    name = 'djdt_template_timings'

    @staticmethod
    def ready():
        from .panel import TemplateTimingsPanel

        TemplateTimingsPanel.install_monkey_patches()
