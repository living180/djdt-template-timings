# Copyright 2013-2018 Tom Forbes
# Copyright 2022 Daniel Harding
# Distributed as part of the djdt-template-timings project under the terms of the MIT
# license.

from django.conf import settings


CONFIG_DEFAULTS = {
    'IGNORED_TEMPLATES': ["debug_toolbar/*"]
}


_config = None


def get_config():
    global _config
    if _config is None:
        _config = {
            **CONFIG_DEFAULTS,
            **getattr(settings, 'DJDT_TEMPLATE_TIMINGS_CONFIG', {}),
        }
    return _config
