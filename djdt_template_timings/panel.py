# Copyright 2013-2018 Tom Forbes
# Copyright 2022 Daniel Harding
# Distributed as part of the djdt-template-timings project under the terms of the MIT
# license.

import fnmatch
import functools
import threading
import time

from collections import defaultdict

from django.template import NodeList, Template
from django.template.loader_tags import BlockNode

from debug_toolbar.panels import Panel
from debug_toolbar.settings import get_panels

if "debug_toolbar.panels.sql.SQLPanel" in get_panels():
    from debug_toolbar.panels.sql import SQLPanel
else:
    SQLPanel = None

from .settings import get_config


_ignored_cache = {}

def template_is_ignored(name):
    ignored = _ignored_cache.get(name)
    if ignored is None:
        ignored = any(
            fnmatch.fnmatch(name, pattern)
            for pattern in get_config()["IGNORED_TEMPLATES"]
        )
        _ignored_cache[name] = ignored
    return ignored


class RenderEntry:
    def __init__(self):
        self.duration = 0
        self.cumulative_duration = 0
        self.query_count = 0
        self.query_duration = 0

    def merge(self, other):
        self.duration += other.duration
        self.query_count += other.query_count
        self.query_duration += other.query_duration


class TemplateEntry:
    def __init__(self, name):
        self.name = name
        self.render_entries = []

    def count(self):
        return len(self.render_entries)

    def duration(self):
        return sum(entry.duration for entry in self.render_entries)

    def cumulative_duration(self):
        return sum(entry.cumulative_duration for entry in self.render_entries)

    def average(self):
        return self.duration() / self.count()

    def cumulative_average(self):
        return self.cumulative_duration() / self.count()

    def minimum(self):
        return min(entry.duration for entry in self.render_entries)

    def cumulative_minimum(self):
        return min(entry.cumulative_duration for entry in self.render_entries)

    def maximum(self):
        return max(entry.duration for entry in self.render_entries)

    def cumulative_maximum(self):
        return max(entry.cumulative_duration for entry in self.render_entries)

    def query_count(self):
        return sum(entry.query_count for entry in self.render_entries)

    def query_duration(self):
        return sum(entry.query_duration for entry in self.render_entries)

    def query_percentage(self):
        return self.query_duration() / self.duration() * 100


class TemplateDict(defaultdict):
    def __missing__(self, key):
        entry = TemplateEntry(key)
        self[key] = entry
        return entry


class TemplateTimingsPanel(Panel):
    panels_by_thread = {}

    @classmethod
    def get_for_thread(cls):
        return cls.panels_by_thread.get(threading.get_ident())

    monkey_patches = []

    @classmethod
    def register_monkey_patch(cls, target, method_name):
        def decorator(f):
            cls.monkey_patches.append((target, method_name, f))
            return f
        return decorator

    @classmethod
    def install_monkey_patch(cls, target, method_name, f):
        original_method = getattr(target, method_name)

        @functools.wraps(original_method)
        def wrapper(self, *args, **kwargs):
            panel = cls.get_for_thread()
            if panel is None:
                return original_method(self, *args, **kwargs)
            return f(panel, original_method, self, args, kwargs)

        setattr(target, method_name, wrapper)

    @classmethod
    def install_monkey_patches(cls):
        for target, method_name, f in cls.monkey_patches:
            cls.install_monkey_patch(target, method_name, f)
        cls.monkey_patches = []

    title = 'Template Timings'
    template = "djdt_template_timings/panel.html"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.recording_sql = False
        self.template_entries = TemplateDict()
        self.template_stack = []
        self.base_duration = 0
        self.in_blocknode_render = False

    def enable_instrumentation(self):
        self.recording_sql = SQLPanel is not None and any(
            isinstance(panel, SQLPanel) for panel in self.toolbar.enabled_panels
        )
        self.panels_by_thread[threading.get_ident()] = self

    def disable_instrumentation(self):
        self.panels_by_thread.pop(threading.get_ident(), None)

    @property
    def nav_subtitle(self):
        subtitle = f"{self.base_duration:.0f} ms"
        if self.recording_sql:
            total_queries = sum(
                entry.query_count() for entry in self.template_entries.values()
            )
            subtitle += f" with {total_queries} queries"

            if total_queries and self.base_duration:
                total_query_duration = sum(
                    entry.query_duration() for entry in self.template_entries.values()
                )
                subtitle += f" ({total_query_duration / self.base_duration:.1%} SQL)"

        return subtitle

    def generate_stats(self, request, response):
        self.record_stats({
            "template_entries": sorted(
                self.template_entries.values(),
                key=lambda entry: entry.cumulative_duration(),
                reverse=True,
            ),
            "recording_sql": self.recording_sql,
        })


def time_template_render(panel, original_method, self, args, kwargs):
    if self.name is None:
        name = '<unknown>'
    else:
        name = self.name

    if template_is_ignored(name):
        return original_method(self, *args, **kwargs)

    template_entry = panel.template_entries[name]
    render_entry = RenderEntry()
    template_entry.render_entries.append(render_entry)

    panel.template_stack.append(template_entry)
    start_time = time.time()
    result = original_method(self, *args, **kwargs)
    end_time = time.time()
    panel.template_stack.pop()

    duration = (end_time - start_time) * 1000

    render_entry.cumulative_duration = duration
    render_entry.duration += duration

    if panel.template_stack:
        panel.template_stack[-1].render_entries[-1].duration -= duration
    else:
        panel.base_duration += duration

    return result


@TemplateTimingsPanel.register_monkey_patch(Template, 'render')
def template_render_wrapper(panel, original_method, self, args, kwargs):
    return time_template_render(panel, original_method, self, args, kwargs)


# The following monkey-patch is to handle rendering of templates via {% extends %} tags.
# ExtendsNode.render() does not call Template.render() but instead calls
# Template._render() directly.  So this monkey-patch wraps Template._render() and if the
# template name does not match the name on the top of the template stack, runs the same
# logic as the Template.render() monkey patch. (If the template name matches the name on
# the top of the template stack, that simply indicates that Template._render() was
# called from Template.render().)
@TemplateTimingsPanel.register_monkey_patch(Template, '_render')
def template__render_wrapper(panel, original_method, self, args, kwargs):
    if panel.template_stack and panel.template_stack[-1].name == self.name:
        return original_method(self, *args, **kwargs)
    else:
        return time_template_render(panel, original_method, self, args, kwargs)


# The following two monkey-patches are a very hacky way to assign the timing for a block
# to the template containing the block.  BlockNode.render() only gets invoked on the
# node from the base template that defines the block, even if another template extends
# the base template and overrides the block.  BlockNode.render() then creates a copy of
# the block from the extending template and calls .render() on that block's nodelist.
# Thus to determine which template the block actually comes from, the first monkey-patch
# of BlockNode.render() pushes a synthetic TemplateEntry with name set to None onto the
# panel's template_stack, and then sets a flag on the panel, in_blocknode_render, before
# calling the original BlockNode.render() method.  The second monkey-patch of
# NodeList.render() checks for the in_blocknode_render flag on the panel.  If the flag
# is set and the NodeList has nodes, it pulls the template name from the first node in
# the NodeList and stores it as the name of the TemplateEntry from the top of the
# panel's template_stack (which was pushed by the first monkey-patch).  Once control
# returns to the first monkey-patch of BlockNode.render(), it then pops the
# TemplateEntry from the panel's template_stack, and if its name is not None, merges its
# RenderEntry with the current RenderEntry from the real TemplateEntry for that template
# further up the stack.

@TemplateTimingsPanel.register_monkey_patch(BlockNode, 'render')
def blocknode_render_wrapper(panel, original_method, self, args, kwargs):
    blocknode_template_entry = TemplateEntry(name=None)
    render_entry = RenderEntry()
    blocknode_template_entry.render_entries.append(render_entry)
    panel.template_stack.append(blocknode_template_entry)

    panel.in_blocknode_render = True
    start_time = time.time()
    result = original_method(self, *args, **kwargs)
    end_time = time.time()

    panel.template_stack.pop()

    if blocknode_template_entry.name is not None:
        duration = (end_time - start_time) * 1000
        render_entry.duration += duration
        panel.template_stack[-1].render_entries[-1].duration -= duration
        for template_entry in reversed(panel.template_stack):
            if template_entry.name == blocknode_template_entry.name:
                template_entry.render_entries[-1].merge(render_entry)
                break
    return result


@TemplateTimingsPanel.register_monkey_patch(NodeList, 'render')
def nodelist_render_wrapper(panel, original_method, self, args, kwargs):
    if panel.in_blocknode_render:
        panel.in_blocknode_render = False
        if self:
            panel.template_stack[-1].name = self[0].origin.template_name
    return original_method(self, *args, **kwargs)


if SQLPanel is not None:
    @TemplateTimingsPanel.register_monkey_patch(SQLPanel, 'record')
    def sqlpanel_record_wrapper(panel, original_method, self, args, kwargs):
        if panel.template_stack and panel.recording_sql:
            render_entry = panel.template_stack[-1].render_entries[-1]
            render_entry.query_count += 1
            render_entry.query_duration += kwargs["duration"]
        return original_method(self, *args, **kwargs)
